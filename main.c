#include <stdio.h>
#include <stdlib.h>
#include "ArvoreAVL.h"

#define tam 25					//tamanho vetor de codigos
#define intervalo 300			//intevalo aleatorio dos codigos

int main(){
    ArvAVL* avl;
    int res;
   
    int dados[tam];
   
    for(int i=0; i < tam; i++)
    	dados[i] = rand() % intervalo;

    avl = cria_ArvAVL();

    for(int i=0;i<tam;i++){
        res = insere_ArvAVL(avl, dados[i]);   
    }
    printf("\nAVL tree in-ordem:\n");
    emOrdem_ArvAVL(avl);
    printf("\n\n");

    printf("\nAVL tree pre-ordem:\n");
    preOrdem_ArvAVL(avl);
    printf("\n\n");
    
    printf("\nAVL tree pos-ordem:\n");
    preOrdem_ArvAVL(avl);
    printf("\n\n");

    libera_ArvAVL(avl);

    return 0;
}
